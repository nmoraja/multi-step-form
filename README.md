# Multi Step Form

HTML, CSS, Vanilla JavaScript multistep form.

### Built with

- HTML5 markup
- SCSS custom properties
- FlexBox
- JavaScript
- Mobile-first workflow

### Screenshot
![](./screenshot.png)

### Links

- Solution URL: [https://nmorajda-multi-step-form.netlify.app](https://nmorajda-multi-step-form.netlify.app)
- Live Site URL: [https://nmorajda-multi-step-form.netlify.app](https://nmorajda-multi-step-form.netlify.app)

## Author

- Website - [N. Morajda](https://abmstudio.pl)
- Frontend Mentor - [@nmorajda](https://www.frontendmentor.io/profile/nmorajda)
- Github - [nmorajda](https://github.com/nmorajda)
- Bitbucket - [nmorajda](https://bitbucket.org/nmoraja/)


