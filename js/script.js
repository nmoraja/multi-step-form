'use strict';

class Multistep {
  constructor(options) {
    // Default settings
    const defaults = {
      selector: '.multistep', // default selector
      dividerSelector: '.multistep__step', // default divider
      showClassName: 'display--inline-flex', // css class from stylesheet
      hiddenClassName: 'display--none', // css class from stylesheet
      validation: false,
      debug: true,
    };

    // Merge user options into defaults
    this.settings = { ...defaults, ...options };
    this.container = document.querySelector(this.settings.selector);

    this.ui = {
      container: this.container,
      steps: this.container.querySelectorAll(this.settings.dividerSelector),
      btnPrev: this.container.querySelector('.multistep__button-prev'),
      btnNext: this.container.querySelector('.multistep__button-next'),
      btnSubmit: this.container.querySelector('[type="submit"]'),
      progressbar: this.container.querySelector('.multistep__progressbar'),
      progressSteps: [],
    };

    this.state = {
      currentStep: 1,
    };

    this.init();
  }

  log() {
    console.log(this);
  }

  updateState(newState) {
    this.state = { ...this.state, ...newState };
    this.updateUi();

    if (this.settings.debug) this.log();
  }

  updateUi() {
    const { showClassName, hiddenClassName } = this.settings;
    const { steps, btnPrev, btnNext, btnSubmit, progressSteps } = this.ui;

    const { currentStep } = this.state;

    const stepsNumber = steps.length;

    if (currentStep === 1) {
      btnPrev.classList.remove(showClassName);
      btnNext.classList.add(showClassName);
      if (btnSubmit) {
        btnSubmit.classList.remove(showClassName);
      }
    }

    if (currentStep > 1) {
      btnNext.classList.add(showClassName);
      btnPrev.classList.add(showClassName);
      if (btnSubmit) {
        btnSubmit.classList.remove(showClassName);
      }
    }

    if (currentStep === stepsNumber) {
      btnNext.classList.remove(showClassName);
      if (btnSubmit) {
        btnSubmit.classList.add(showClassName);
      }
    }

    // clear active class
    this.removeClass('multistep__step--active', steps);
    this.removeClass('multistep__progressbar__step--active', progressSteps);

    const currentIndex = currentStep - 1;

    steps[currentIndex].classList.add('multistep__step--active');

    if (currentIndex > 0) {
      steps[currentIndex - 1].classList.add(hiddenClassName);
    }

    progressSteps[currentIndex].classList.add(
      'multistep__progressbar__step--active'
    );

    document.querySelector('.multistep__progressbar__line').style.width =
      ((currentStep - 1) / (progressSteps.length - 1)) * 100 + '%';

    //   focus na pierwszy element typu input
    const firstInput = steps[currentIndex].querySelector(
      '.multistep__field input'
    );
    if (firstInput) firstInput.focus();
  }

  createProgressBar() {
    const { steps, progressbar, progressSteps } = this.ui;

    const elemLine = document.createElement('hr');
    elemLine.classList.add('multistep__progressbar__line');

    progressbar.insertAdjacentElement('afterbegin', elemLine);

    steps.forEach((step, index) => {
      // const title = step.querySelector('legend').textContent;

      const elemStep = document.createElement('span');
      elemStep.classList.add('multistep__progressbar__step');
      // elemStep.setAttribute('title', title);
      elemStep.textContent = index + 1;

      if (index === 0)
        elemStep.classList.add('multistep__progressbar__step--active');

      progressSteps.push(elemStep);
      progressbar.insertAdjacentElement('beforeend', elemStep);
    });
  }

  removeClass(className, elements) {
    if (!className) return;
    if (!elements) return;

    elements.forEach(elem => {
      elem.classList.remove(className);
    });
  }

  init() {
    const { showClassName, hiddenClassName } = this.settings;
    const { container, steps, progressbar, btnNext, btnPrev, btnSubmit } =
      this.ui;

    const stepsNumber = steps.length;

    if (steps.length <= 1) {
      if (progressbar) progressbar.classList.add(hiddenClassName);
      return;
    }

    // hidden steps
    for (let i = 1; i < steps.length; i++) {
      steps[i].classList.add(hiddenClassName);
    }

    // create progres bar
    if (progressbar) this.createProgressBar();

    // show button
    if (btnNext) {
      btnNext.classList.add('display--inline-flex');

      btnNext.addEventListener('click', () => {
        if (this.state.currentStep >= stepsNumber) return;

        if (
          this.settings.validation &&
          !this.settings.validation(steps[this.state.currentStep - 1])
        )
          return;

        this.updateState({ currentStep: this.state.currentStep + 1 });
      });
    }

    if (btnPrev) {
      btnPrev.addEventListener('click', () => {
        if (this.state.currentStep <= 1) return;

        this.updateState({ currentStep: this.state.currentStep - 1 });
      });
    }

    // disabled browser validation
    container.setAttribute('novalidate', true);

    // hidden submit
    if (btnSubmit) btnSubmit.classList.add(hiddenClassName);
  }
}

function formValidate(form) {
  const inpRequired = form.querySelectorAll('input:required');

  let isValid = true;

  inpRequired.forEach(inp => {
    if (!inp.validity.valid) {
      isValid = false;
    }

    inp.validity.valid ? showSuccess(inp) : showError(inp);
  });

  return isValid;
}

function showSuccess(input) {
  input.parentElement.className = 'form__field form__field--success';
}

function showError(input) {
  input.parentElement.className = 'form__field form__field--error';
}

const registerForm = document.querySelector('#registerForm');
registerForm.setAttribute('novalidate', true);

registerForm.addEventListener('submit', function registerHandler(e) {
  e.preventDefault();

  if (!formValidate(registerForm)) {
    console.log('no validate form');
    return;
  }

  console.log('send form...');
});

const multistepRegisterForm = new Multistep({
  selector: '#registerForm',
  validation: formValidate,
});

// events
window.addEventListener('load', function loadHandler() {
  // klasa preload ukrywa animacje transition podczas ładowania dokumentu
  // zobacz definicje klasy w pliku scss/css
  document.body.classList.remove('preload');

  this.removeEventListener('load', loadHandler);

  // change theme support

  // change theme selectors
  const inpsSwitchTheme = document.querySelectorAll(
    '.theme__selector input[type="checkbox"]'
  );

  const removeCheckedState = inps => {
    inps.forEach(inp => {
      inp.checked = false;
    });
  };

  inpsSwitchTheme.forEach(inp => {
    inp.addEventListener('change', () => {
      removeCheckedState(inpsSwitchTheme);
      inp.checked = true;
      document.body.className = '';
      document.body.classList.add(inp.value);
    });
  });
});
